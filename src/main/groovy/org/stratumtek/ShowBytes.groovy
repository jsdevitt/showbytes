package org.stratumtek
/**
 * Write a function to convert an Integer representing a number of bytes (less than or 
 * equal to 1 Gigabyte) into an easy to read format, defined as follows:
 * * Maximum of 3 digits (not counting a decimal point), and a single letter to signify the unit of measure.
 * * No leading zeroes, or trailing zeroes after a decimal point.
 * * Be as accurate as possible.
 * IMPORTANT DETAILS:
 * * Maximum of 3 digits (not counting a decimal point), and a single letter to signify the unit of measure.
 * * Round to the nearest valid values.
 * 
 * Examples:
 * * 341 = 341B
 * * 34200 = 34.2K
 * * 5910000 = 5.91M
 * * 1000000000 = 1G
 * * No leading zeroes, or trailing zeroes after a decimal point. 
 *
 * Examples:
 * o 34K, not 034K
 * o 7.2M, not 7.20M
 * o Be as accurate as possible. Example:
 * o 54123B = 54.1K, not 54K
 * o Note: For this problem, 1000 bytes = 1 KB, and so on.
 * @author sdevitt
 *
 */
class ShowBytes {
	
	// default number of signficant digits
	static Integer precision = 3
	static Integer MAX_SIZE=9
	// Used to generate prefix strings of zeros
	static final String PADDING = "000000000000000000000" // 
	// Upper bound on number of digits in a byte string

	
	public static void main(List argv) {
		ShowBytes sb = new ShowBytes()
		def nstring = parseArgs(argv)
		try {
			sb.exec(nstring)
		} catch ( Exception e) {	
			println( "ERROR: failed to process $nstring")
			System.exit(1)
		}
		System.exit(0)
	}
	
	public List parseArgs(List argList ) {
		assert argList.size() == 1 , "usage: showbytes <number_of_bytes>"
		return argList[0]
	}
	public void exec(String nstring) {
		def result = showSize(nstring)
		println "${nstring} -> ${result}"
		return
	}
	/**
	 * Compute a string representation of a number of bytes using BKMG notation
	 * Use a string representation to stay within normal size of Long and integers
	 * @param s
	 * @return a shortened String representation
	 */
	
	String showSize( String s) {
		if (! validate(s)) {
			throw new Exception( "invalid data")
		}
		def candidate = roundAsString(s,ShowBytes.precision )
		def type = scalingFactor(candidate)
		switch(type) {
			case 'K':
				candidate = padToN(candidate , 6)
				break;
			case 'M':
				candidate = padToN(candidate , 9)
				break;
			case 'G':
				candidate = padToN(candidate,12)
				break
			default:
				candidate = padToN( candidate , 3)
		}
		// println( "$s -> $candidate, type=$type")
		def num = format( candidate )
		return( "${num}${type}")
	}
	/**
	 * Validate that we have a string representation of an integer
	 * @param s
	 * @return true or false
	 */
	boolean validate(s) {
		if (s == null) return false
		def result = true;
		def candidate = stripLeadingZeros(s)
		if (candidate.size() > 9 && s != "1000000000" ) result = false
		else { 
			for (c in candidate) {		
				try {
					def n = c.toInteger()
				} catch (Exception e) {
					result = false
				}
			}
		}
		return result
	}
	/**
	 * A helper method to compute terminating qualifier for output string
	 * 
	 * @param n the integer to be analyzed
	 * @return a single character String 
	 */
	 String scalingFactor(String s) {
		def candidate = stripLeadingZeros(s)
		if (candidate.size() < 4) return "B"
		else if ( candidate.size() < 7 ) return "K"
		else if ( candidate.size() < 10 ) return "M"
		else if ( candidate.size() < 13 ) return "G"
		else return "T"   // terabytes
	}
	 
	/**
	 * Rewrite s as an equivalent string rounded n significant digits
	 * The implementation uses integer rounding so there is an upper limit
	 * on the number of significant digits used for rounding
	 * There is no leading zeros in the result.
	 * 
	 * @param s the integer as a string
	 * @param n the number of 
	 * @return a modified string with the first n digits significant
	 */	 
	 static String roundAsString( String s , Integer n) {
		 def candidate = stripLeadingZeros(s)
		 def maxSize = ShowBytes.PADDING.size()
		 assert n && n>0 && n < 8 , "Truncation size $n not available"
		 assert candidate.size() < maxSize , "Number $s of size bigger than $maxSize not supported"
		 // first deal with short strings
		 def len = candidate.size() 
		 if ( len <= n )  return candidate		
		 def body = candidate[0..n-1]
		 def tail = ShowBytes.PADDING[0..(len-n-1)]  // all zeros
		 def last = candidate[n].toInteger()
		 def result = "${body}${tail}"
		 // round if necessary
		if (last > 4) {  //rounding required so adjust body and recurse
			body = (body.toInteger()+1).toString()
			result = roundAsString("${body}${tail}",n)
		} 
		return result
	 }
	 
	 /**
	  * Strip off leading zeros, adjusting for the special case of "0"
	  * @param s
	  * @return s modified to have no leading zeros
	  */
	 static String stripLeadingZeros(String s) {
		if (! s ) return "0"
		int i
		for (i = 0; i< s.size() ; i++ ){ if ( s[i] != "0" ) break }
		if (i == s.size()) return "0"
		def candidate = s[i..-1]
		return candidate
	 }

	 /**
	  * Prefix "s" with n zeros.
	  */
	 static String padToN(String s,Integer n) {
		 def prefix = "";
		 if ( s.size() < n)  {
			 prefix =  ShowBytes.PADDING[0..(n-s.size()-1)]
		 }
		 return "${prefix}${s}"
	 }
	 
	 /**
	 * Format the leading 4 characters of a number for printing
	 * as per the problem specification.  The data should be padded by zeros
	 * to one of the standard sizes (B,K,M,G) so that the 4th
	 * character is the fractional part, when needed.
	 * 
	 * Assumptions: string representation of a number in the 
	 * the range of 0 .. 9999, padded with leading zeros to align on a scaling factor
	 * boundary.   The 4th digit is the fractional part.
	 * 
	 * @param s  A string representation of an integer
	 * @return a list of the two parts (number,fraction).
	 */
	static String format(String s) {
		// assert s.size() == 4, "Formatting requires a 4 digit string"
		int n = s[0..2].toInteger()
		def body = "$n"
		def tailing = ""
		if ( s.size() < 4 ) { // unscaled byte case
			return body
		}
		// We may have a fractional part that we can use
		switch(n) {
			case { it > 99 } :
				// available digits all used up
				break
			case { it > 9 } :
				// one possible fraction digit
				tailing = s[3]				
				break
			case { it  >= 1 } :
				// two possible fraction digits
				tailing = s.size() > 4 ? s[3..4] : s[3]
				if ( tailing.endsWith("0") ) tailing = s[3]
				break
			default :
				throw new Exception( "Size of $s -> $n must be at least 1")
				break
		}
		if (tailing == "0") tailing = ""
		if ( tailing )  tailing = ".${tailing}"
		return "${body}${tailing}"
	}

}
