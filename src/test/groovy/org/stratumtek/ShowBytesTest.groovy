package org.stratumtek;

import static org.junit.Assert.*;
import org.junit.*;


import org.junit.Test;

class ShowBytesTest {
	/**
	 * * 341 = 341B
	 * * 34200 = 34.2K
	 * * 5910000 = 5.91M = 1G
	 * * No leading zeroes, or trailing zeroes after a decimal point. 
	 */
	private showBytes
	@Before
	public void setUp() {
		showBytes = new ShowBytes()
	}

	@Test
	public void testStripLeadingZeros() {
		showBytes.stripLeadingZeros("0") == "0"
		showBytes.stripLeadingZeros("01") == "1"
		showBytes.stripLeadingZeros("1") == "1"
		showBytes.stripLeadingZeros("010") == "10"
	}

	@Test
	public void testPadToN() {
		showBytes.padToN("1",4) == "0001"
		showBytes.padToN("1",9) == "000000001"
		showBytes.padToN("0",9) == "000000000"
		showBytes.padToN("1",6) == "000001"
	}
	
	@Test
	public void testRoundAsString() {
		assert showBytes.roundAsString("994",ShowBytes.precision + 1) == "994"
		assert showBytes.roundAsString("1",1) == "1"
		assert showBytes.roundAsString("2",2) == "2"
		assert showBytes.roundAsString("1",4) == "1"
		assert showBytes.roundAsString("01",4) == "1"
		assert showBytes.roundAsString("001",4) == "1"
		assert showBytes.roundAsString("0010",4) == "10"
		assert showBytes.roundAsString("9",1) == "9"
		assert showBytes.roundAsString("9",2) == "9"
		assert showBytes.roundAsString("34200",3) == "34200"
		assert showBytes.roundAsString("0034200",3) == "34200"
		// assert showBytes.roundAsString("00001",4) == "0001"
		assert showBytes.roundAsString("999",3) == "999"
		assert showBytes.roundAsString("9994",3) == "9990"
		assert showBytes.roundAsString("9995",3) == "10000"
		assert showBytes.roundAsString("8994",3) == "8990"
		assert showBytes.roundAsString("222251000",4)  == "222300000"
		assert showBytes.roundAsString("0222251000",4) == "222300000"
		assert showBytes.roundAsString("0222251000",5) == "222250000"
		assert showBytes.roundAsString("999951000",4) == "1000000000"
		assert showBytes.roundAsString("40150",4) == "40150"
		assert showBytes.roundAsString("40150",3) == "40200"
		assert showBytes.roundAsString("40140",3) == "40100"
	}

	@Test
	public void testValidate() {
		showBytes = new ShowBytes()
		assert showBytes.validate("999") == true
		assert showBytes.validate("-999") == false
		assert showBytes.validate("aaa") == false
		assert showBytes.validate("99999") == true
		assert showBytes.validate("999999999") == true
		assert showBytes.validate("999999999888888888888") == false
	}
	@Test
	public void testScalingFactor() {
		assert showBytes.scalingFactor("0") == "B"
		assert showBytes.scalingFactor("1") == "B"
		assert showBytes.scalingFactor("123") == "B"
		assert showBytes.scalingFactor("9999") == "K"
		assert showBytes.scalingFactor("99999") == "K"
		assert showBytes.scalingFactor("34200") == "K"
		assert showBytes.scalingFactor("999999999") == "M"
		assert showBytes.scalingFactor("1000000000") == "G"
	}
	
	@Test
	public void testFormat() {
		// digits beyond 3 are fractions
		showBytes = new ShowBytes()
		assert showBytes.format("999") == "999"
		assert showBytes.format("23456") == "234" 
		assert showBytes.format("02345") == "23.4" 
		assert showBytes.format("0125") == "12.5" 
		assert showBytes.format("01253") == "12.5" 
		assert showBytes.format("012533") == "12.5" 
		assert showBytes.format("0012") == "1.2" 
		assert showBytes.format("00125") == "1.25" 
		assert showBytes.format("001256") == "1.25" 
		assert showBytes.format("0012566") == "1.25" 
		assert showBytes.format("1235") == "123" 
		// assert showBytes.format("10") == "1." 
		assert 1 == 1
	}
/*
*/

	@Test
	public void testShowSize() {
		showBytes = new ShowBytes()
		assert showBytes.showSize("994") == "994B"
		assert showBytes.showSize("999") == "999B"
		assert showBytes.showSize("9999") == "10K"
		assert showBytes.showSize("4444") == "4.44K"
		assert showBytes.showSize("4445") == "4.45K"
		assert showBytes.showSize("40140") == "40.1K"
		assert showBytes.showSize("40150") == "40.2K"
		assert showBytes.showSize("40950") == "41K"
		assert showBytes.showSize("1000000") == "1M"
		assert showBytes.showSize("999999") == "1M"
		
		assert showBytes.showSize("34200") == "34.2K"
		assert showBytes.showSize("5910000") == "5.91M"
		assert showBytes.showSize("1000000000") == "1G"
		
				// fail("Not yet implemented");
		// assert showBytes.showSize( 341 ) == "341B"
	}

}
